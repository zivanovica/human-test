<?php
/**
 * Created by IntelliJ IDEA.
 * User: coa
 * Date: 9/22/17
 * Time: 3:51 PM
 */

/**
 * As we are not using composer, we'll setup basic autoload
 */
spl_autoload_register(function ($class) {

    $classFilePath = __DIR__ . '/' . str_replace('\\', '/', trim($class, '\\')) . '.php';

    if (false === is_readable($classFilePath)) {

        throw new RuntimeException("Invalid class file path {$classFilePath}");
    }


    require_once $classFilePath;
});

$male = new Human('John', 25, \Human\Body::GENDER_MALE);
$female = new Human('Jessica', 22, \Human\Body::GENDER_FEMALE);



var_dump($male->isAlive());