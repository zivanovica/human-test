<?php

namespace Human\Facades;

/**
 * Created by IntelliJ IDEA.
 * User: coa
 * Date: 9/22/17
 * Time: 4:16 PM
 */
interface IIsUsable
{

    /**
     *
     * Retrieve information regarding health of object that implements this interface
     *
     * @return bool
     */
    public function isUsable();
}