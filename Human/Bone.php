<?php

namespace Human;

use Human\Facades\IIsUsable;

/**
 * Created by IntelliJ IDEA.
 * User: coa
 * Date: 9/22/17
 * Time: 2:25 PM
 */
abstract class Bone implements IIsUsable
{

    /** @var bool Flag that determines is bone broken or not */
    protected $_isBroken;

    /** @var bool Flag that determines is bone required to be healthy in order for human ti be alive */
    protected $_isRequired;

    /**
     * @param bool $isBroken
     * @param bool $isRequired Flag that determines is this bone required for human to be alive (e.g Spine is required)
     */
    public function __construct($isBroken = false, $isRequired = false)
    {

        $this->_isBroken = $isBroken;

        $this->_isRequired = $isRequired;
    }

    /**
     *
     * Determines is bone usable
     *
     * @return bool
     */
    public function isUsable()
    {

        return false === ($this->_isBroken && $this->_isRequired);
    }

    /**
     *
     * Retrieve information regarding bone status, TRUE if broken FALSE otherwise
     *
     * @return bool
     */
    public function isBroken()
    {

        return $this->_isBroken;
    }

    /**
     *
     * Retrieve flag that determines is bone required for human to be alive
     *
     * @return bool
     */
    public function isRequired()
    {

        return $this->_isRequired;
    }
}