<?php
/**
 * Created by IntelliJ IDEA.
 * User: coa
 * Date: 9/22/17
 * Time: 4:08 PM
 */


// THIS SHOULD MAYBE BE PART OF BACK INSTEAD OF TORSO....
namespace Human\Bones\Torso;

use Human\Bone;

class Spine extends Bone
{

    public function __construct($isBroken = false)
    {
        parent::__construct($isBroken, true);
    }
}