<?php
/**
 * Created by IntelliJ IDEA.
 * User: coa
 * Date: 9/22/17
 * Time: 4:39 PM
 */

namespace Human\Bones\Torso;

use Human\Bone;

class Chest extends Bone
{

    public function __construct($isBroken = false)
    {

        parent::__construct($isBroken, true);
    }
}