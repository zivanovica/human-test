<?php

namespace Human\Bones\Head;

use Human\Bone;

/**
 * Created by IntelliJ IDEA.
 * User: coa
 * Date: 9/22/17
 * Time: 4:07 PM
 */
class Skull extends Bone
{

    public function __construct($isBroken = false)
    {

        parent::__construct($isBroken, true);
    }
}