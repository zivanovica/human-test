<?php
/**
 * Created by IntelliJ IDEA.
 * User: coa
 * Date: 9/22/17
 * Time: 4:12 PM
 */

namespace Human\Bones\Head;

use Human\Bone;

class Jaw extends Bone
{

    public function __construct($isBroken = false)
    {

        parent::__construct($isBroken, false);
    }
}