<?php

namespace Human\Bones\Arm;

use Human\Bone;

/**
 * Created by IntelliJ IDEA.
 * User: coa
 * Date: 9/22/17
 * Time: 4:34 PM
 */
class ForeArm extends Bone
{

    public function __construct($isBroken = false)
    {

        parent::__construct($isBroken, false);
    }
}