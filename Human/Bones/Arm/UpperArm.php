<?php
/**
 * Created by IntelliJ IDEA.
 * User: coa
 * Date: 9/22/17
 * Time: 4:35 PM
 */

namespace Human\Bones\Arm;

use Human\Bone;

class UpperArm extends Bone
{

    public function __construct($isBroken = false)
    {

        parent::__construct($isBroken, false);
    }
}