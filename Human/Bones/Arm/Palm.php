<?php
/**
 * Created by IntelliJ IDEA.
 * User: coa
 * Date: 9/22/17
 * Time: 4:38 PM
 */

namespace Human\Bones\Arm;


use Human\Bone;

class Palm extends Bone
{

    public function __construct($isBroken = false)
    {
        parent::__construct($isBroken, false);
    }
}