<?php
/**
 * Created by IntelliJ IDEA.
 * User: coa
 * Date: 9/22/17
 * Time: 4:45 PM
 */

namespace Human\Bones\Leg;


use Human\Bone;

class Foot extends Bone
{

    public function __construct($isBroken = false)
    {

        parent::__construct($isBroken, false);
    }
}