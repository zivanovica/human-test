<?php
/**
 * Created by IntelliJ IDEA.
 * User: coa
 * Date: 9/22/17
 * Time: 4:10 PM
 */

namespace Human\BodyParts;

use Human\BodyPart;
use Human\Bones\Leg\Foot;
use Human\Bones\Leg\LowerLeg;
use Human\Bones\Leg\UpperLeg;

class Legs extends BodyPart
{

    public function __construct()
    {
        parent::__construct(
            [],
            [
                // LEFT LEG
                new UpperLeg(),
                new LowerLeg(),
                new Foot(),
                // RIGHT LEG
                new UpperLeg(),
                new LowerLeg(),
                new Foot(),
            ]
        );
    }
}