<?php
/**
 * Created by IntelliJ IDEA.
 * User: coa
 * Date: 9/22/17
 * Time: 4:10 PM
 */

namespace Human\BodyParts;

use Human\BodyPart;
use Human\Bones\Torso\Chest;
use Human\Bones\Torso\Spine;
use Human\Organs\Hearth;
use Human\Organs\Kidney;
use Human\Organs\Liver;

class Torso extends BodyPart
{

    public function __construct()
    {
        parent::__construct(
            [
                new Hearth(),
                new Liver(),
                new Kidney(), // Left kidney
                new Kidney(), // Right kidney
            ],
            [
                new Spine(), // If spine is broken then human is not alive
                new Chest() // If chest is broken then human is not alive
            ]
        );
    }
}