<?php

namespace Human\BodyParts;

use Human\BodyPart;
use Human\Bones\Head\Jaw;
use Human\Bones\Head\Skull;
use Human\Organs\Head\Ear;
use Human\Organs\Head\Eye;
use Human\Organs\Head\Mouth;

/**
 * Created by IntelliJ IDEA.
 * User: coa
 * Date: 9/22/17
 * Time: 3:45 PM
 */
class Head extends BodyPart
{

    public function __construct()
    {
        parent::__construct(
            [
                new Eye(), // Left Eye
                new Eye(), // Right Eye
                new Ear(), // Left Ear
                new Ear(), // Right Ear
                new Mouth() // Mouth
            ],
            [
                new Skull(), // If skull is broken then human is not alive
                new Jaw(),
            ]
        );
    }
}