<?php
/**
 * Created by IntelliJ IDEA.
 * User: coa
 * Date: 9/22/17
 * Time: 4:10 PM
 */

namespace Human\BodyParts;

use Human\BodyPart;
use Human\Bones\Arm\ForeArm;
use Human\Bones\Arm\Palm;
use Human\Bones\Arm\UpperArm;

class Arms extends BodyPart
{

    public function __construct()
    {
        parent::__construct(
            [],
            [
                new UpperArm(),
                new ForeArm(),
                new Palm()
            ]
        );
    }
}