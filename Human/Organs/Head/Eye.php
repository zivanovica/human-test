<?php

namespace Human\Organs\Head;

use Human\Organ;

/**
 * Created by IntelliJ IDEA.
 * User: coa
 * Date: 9/22/17
 * Time: 3:42 PM
 */
class Eye extends Organ
{

    public function __construct($health = 100.0)
    {
        parent::__construct(false, $health, 100.0);
    }
}