<?php
/**
 * Created by IntelliJ IDEA.
 * User: coa
 * Date: 9/22/17
 * Time: 4:10 PM
 */

namespace Human\Organs\Head;

use Human\Organ;

class Ear extends Organ
{

    public function __construct($health = 100.0)
    {

        parent::__construct(false, $health, 100.0);
    }
}