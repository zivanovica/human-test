<?php

namespace Human;

use Human\Exceptions\HumanBodyPartException;
use Human\Facades\IIsUsable;

/**
 * Created by IntelliJ IDEA.
 * User: coa
 * Date: 9/22/17
 * Time: 2:22 PM
 */
abstract class BodyPart implements IIsUsable
{

    /** @var Bone[] */
    private $_bones = [];

    /** @var Organ[] */
    private $_organs = [];

    /**
     *
     * @param array $organs
     * @param array $bones
     * @throws HumanBodyPartException
     */
    public function __construct(array $organs, array $bones)
    {

        foreach ($organs as $organ) {

            if (false === $organ instanceof Organ) {

                throw new HumanBodyPartException('Given value is not Organ');
            }

            $this->_organs[] = $organ;
        }

        foreach ($bones as $bone) {

            if (false === $bone instanceof Bone) {

                throw new HumanBodyPartException('Given value is not Bone');
            }

            $this->_bones[] = $bone;
        }
    }

    /**
     * @return bool
     */
    public function isUsable()
    {

        foreach ($this->_organs as $organ) {

            if (false === $organ->isUsable()) {

                return false;
            }
        }

        foreach ($this->_bones as $bone) {

            if (false === $bone->isUsable()) {

                return false;
            }
        }

        return true;
    }
}