<?php

namespace Human;

use Human\Exceptions\HumanBodyException;
use Human\Facades\IIsUsable;

/**
 * Created by IntelliJ IDEA.
 * User: coa
 * Date: 9/22/17
 * Time: 2:30 PM
 */
class Body implements IIsUsable
{

    const GENDER_MALE = 'male';
    const GENDER_FEMALE = 'female';

    /** @var string */
    protected $_gender;

    /** @var BodyPart[] */
    protected $_bodyParts = [];

    /**
     * Body constructor.
     * @param string $gender It can be either 'male' or 'female'
     * @param array $bodyParts
     * @throws HumanBodyException
     */
    public function __construct($gender, array $bodyParts)
    {

        foreach ($bodyParts as $bodyPart) {

            if (false === $bodyPart instanceof BodyPart) {

                throw new HumanBodyException('Value is not BodyPart');
            }

            $this->_bodyParts[] = $bodyPart;
        }

        switch ($gender) {
            case Body::GENDER_MALE:
            case Body::GENDER_FEMALE:

                $this->_gender = $gender;

                break;
            default:
                throw new HumanBodyException("Unknown gender {$gender}");
        }
    }

    /**
     *
     * Retrieve gender of current body
     *
     * @return string
     */
    public function getGender()
    {

        return $this->_gender;
    }

    public function isUsable()
    {

        foreach ($this->_bodyParts as $bodyPart) {

            if (false === $bodyPart->isUsable()) {

                return false;
            }
        }

        return true;
    }

    /**
     *
     * Checks is current body gender male
     *
     * @return bool
     */
    public function isMale()
    {

        return $this->_isGender(Body::GENDER_MALE);
    }

    /**
     *
     * Checks is current body gender female
     *
     * @return bool
     */
    public function isFemale()
    {

        return $this->_isGender(Body::GENDER_FEMALE);
    }


    /**
     *
     * Checks is current body provided gender
     *
     * @param string $gender
     * @return bool
     */
    private function _isGender($gender)
    {

        return $gender === $this->_gender;
    }
}