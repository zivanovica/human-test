<?php

namespace Human;

use Human\Facades\IIsUsable;

/**
 * Created by IntelliJ IDEA.
 * User: coa
 * Date: 9/22/17
 * Time: 2:23 PM
 */
abstract class Organ implements IIsUsable
{

    const MAXIMUM_DAMAGE = 100;

    /** @var bool flag that determines is organ required for human in order to be alive */
    protected $_required;

    /** @var float flag that determines how healthy organ is */
    protected $_health;

    /** @var float */
    protected $_maxHealth;

    /**
     * Organ constructor.
     * @param bool $required Default is TRUE
     * @param float $health Organ health, default is TRUE
     * @param float $maxHealth Maximum amount of health for organ
     */
    protected function __construct($required = true, $health = 100.0, $maxHealth = 100.0)
    {

        $this->_required = $required;

        $this->_health = $health;

        $this->_maxHealth = $maxHealth;
    }

    /**
     *
     * Sets new health value
     *
     * @param $health
     */
    public function setHealth($health)
    {

        $this->_health = ($health < 0 ? 0 : ($health > $this->_maxHealth ? $this->_maxHealth : $health));
    }

    /**
     *
     * Retrieve information regarding Organ usability, if its damaged but not required it returns true
     *
     * @return bool
     */
    public function isUsable()
    {

        return false === ($this->isRequired() && $this->getDamage() === Organ::MAXIMUM_DAMAGE);
    }

    /**
     *
     * Retrieve flag that determines is this organ required for human to be alive
     *
     * @return bool
     */
    public function isRequired()
    {

        return $this->_required;
    }

    /**
     *
     * Get damage percent of an organ
     *
     * @return float
     */
    public function getDamage()
    {

        return $this->_health === 0 ? Organ::MAXIMUM_DAMAGE : ($this->_health / $this->_maxHealth) * 100;
    }
}