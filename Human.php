<?php
use Human\Body;
use Human\BodyParts\Arms;
use Human\BodyParts\Head;
use Human\BodyParts\Legs;
use Human\BodyParts\Torso;

/**
 * Created by IntelliJ IDEA.
 * User: coa
 * Date: 9/22/17
 * Time: 2:22 PM
 */
class Human
{


    /** @var Body */
    private $_body;

    private $_name;

    public function __construct($name, $age, $gender = Body::GENDER_MALE)
    {

        $this->_name = $name;

        $this->_body = new Body(
            $gender,
            [
                new Head(),
                new Torso(),
                new Arms(),
                new Legs()
            ]
        );
    }

    public function isAlive()
    {

        return $this->_body->isUsable();
    }
}